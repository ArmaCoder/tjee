<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pl.arma.entity.Substance" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
	editedSubstance contains the currently edited Substance record.
--%>
<jsp:useBean id="editedSubstance" class="pl.arma.entity.Substance" scope="request" />
<%-- 
	The header.jsp template accepts a list of ;-delimited 
	scripts that will be included in the HEAD section.
--%>
<jsp:include page="templates/header.jsp">
	<jsp:param name="includeScripts" value="substanceValidator.js" />
</jsp:include>

<div id="formContainer">
	<div class="appHeader">
		<%--
			Display a correct page heading.
		--%>
		<c:choose>
			<c:when test="${not empty originalSubstanceCode}">
				Edycja substancji: <c:out value="${originalSubstanceCode}" />
			</c:when>
			<c:otherwise>
				Dodawanie nowej substancji
			</c:otherwise>
		</c:choose>
	</div>
	<form id="appSubstanceForm" action="substance" method="post">
		<table>
			<tbody>
				<tr>
					<td><label for="codeField">Kod substancji:</label>
					<td><input type="text" id="codeField" name="code" value="${editedSubstance.code}" />
						<div id="formCodeErrorMessage" style="display:none"></div>
				</tr>
				<tr>
					<td><label for="nameField">Nazwa substancji:</label>
					<td><input type="text" id="nameField" name="name" value="${editedSubstance.name}" />
						<div id="formNameErrorMessage" style="display:none"></div>
				</tr>
				<tr>
					<td><label for="usageField">Zastosowanie:</label>
					<td><textarea id="usageField" name="usage">${editedSubstance.usage}</textarea>
				</tr>
				<tr>
					<td><label for="annotationsField">Uwagi:</label>
					<td><textarea id="annotationsField" name="annotations">${editedSubstance.annotations}</textarea>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Zapisz zmiany" />
				</tr>
			</tbody>
		</table>
		<%--
			THIS IS IMPORTANT: The original code of an edited substance
			MUST be included in data sent in requests to DataServlet.
			This is the only way to determine what record to update
			after the code was changed by the user.
		--%>
		<c:if test="${not empty originalSubstanceCode}">
			<input type="hidden" name="srcCode" value="${originalSubstanceCode}" />
		</c:if>
	</form>
</div>

<jsp:include page="templates/footer.jsp" />
