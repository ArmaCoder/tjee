<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pl.arma.entity.Substance, pl.arma.helpers.FindHelper, java.util.ArrayList" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	substanceDb is the application-scope database instance.
--%>
<jsp:useBean id="substanceDb" class="pl.arma.db.SubstanceDatabase" scope="application" />
<%-- 
	The header.jsp template accepts a list of ;-delimited 
	scripts that will be included in the HEAD section.
--%>
<jsp:include page="templates/header.jsp">
	<jsp:param name="includeScripts" value="substanceIndex.js" />
</jsp:include>

<div id="appSubstanceList">
	<div class="appHeader">
		<span>Lista chemicznych dodatków do żywności</span>
		<button id="appNewButton" class="appButton" onclick="SubstanceIndex.createNewEntry()"></button>
	</div>
	<table id="appSubstanceListTable">
		<thead>
			<tr><th>Kod<th>Nazwa substancji<th>Usuń</tr>
		</thead>
		<tbody>
		<%	ArrayList<Substance> requestedSubstances;
			String whatToSearch = request.getParameter("what");
	
			if (whatToSearch != null)
				requestedSubstances = new FindHelper(substanceDb).find(whatToSearch);
			else
				requestedSubstances = substanceDb.getListByCode();
		
			for (int substanceNo = 0; substanceNo < requestedSubstances.size(); substanceNo++)
			{
				String tableRowId = "substanceListEntry" + substanceNo;
				Substance s = requestedSubstances.get(substanceNo);
		%>
				<tr id="<%= tableRowId %>">
					<td><a href="substance?srcCode=<%= s.getCode() %>"><%= s.getCode() %></a>
					<td><a href="substance?srcCode=<%= s.getCode() %>"><%= s.getName() %></a>
					<td style="text-align:center">
						<button class="appButton appDeleteButton" 
							onclick="SubstanceIndex.performDelete('<%= s.getCode() %>', '<%= tableRowId %>')">
						</button>
				</tr>
		<%	} /* END FOR */ %>
		</tbody>
	</table>
</div>

<jsp:include page="templates/footer.jsp" />
