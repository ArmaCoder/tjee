//
//	SubstanceIndex namespace
//	Contains functions that handle everything related to the index
//
var SubstanceIndex = {
	// A table this object works on
	table: null,

	// Message strings for various events
	messages : {
		noRows:			'Brak rekordów.',
		delSuccess:		'Usunięto wybraną substancję.',
		delNotFound:	'Nie znaleziono substancji w bazie.',
		delBadRequest:	'Błędne żądanie (prawdopodobnie brak kodu).',
		delUnknown:		'Wystąpił nieznany błąd.'
	},

	// Checks if the table has no substances
	// If empty, adds a row with a "nothing found" information.
	checkIfEmpty: function() {
		var tableBody = this.table.getElementsByTagName("TBODY")[0];
		var tableRows = tableBody.getElementsByTagName("TR");
		
		if (tableRows == null || tableRows.length == 0)
		{
			var infoRow = document.createElement("TR");
			var infoCell = document.createElement("TD");
			
			infoCell.setAttribute("colspan", "3");
			infoCell.style.textAlign = "center";
			infoCell.innerHTML = this.messages.noRows;
			
			tableBody.appendChild(
				infoRow.appendChild(infoCell)
			);
		}
	},
	
	// Removes a specified row from the table
	removeFromTable: function(rowID) {
		var tableBody = this.table.getElementsByTagName("TBODY")[0];
		var rowElement = document.getElementById(rowID);
		
		if (rowElement != null)
			tableBody.removeChild(rowElement);
		
		// Add the 'no records' row if empty
		this.checkIfEmpty();
	},
	
	// Goes to the 'Add new substance' form
	createNewEntry : function() {
		window.location.replace("form.jsp");
	},
	
	// Shows a Message bubble on the top of the website
	showMessage : function (msg, isError) {
		var mainContainerElement = document.getElementById('appMainContainer');
		var bubbleElement = document.createElement('DIV');
		
		bubbleElement.className = isError ? 'appErrorMessage' : 'appSuccessMessage';
		bubbleElement.innerHTML = msg;
		bubbleElement.onclick = function (e) {
			var parentElement = e.target.parentNode;
			parentElement.removeChild(e.target);
		};
		
		mainContainerElement.insertBefore(
			bubbleElement,
			mainContainerElement.firstChild
		);
	},
	
	// Sends the DELETE request to the DataServlet
	// Depending on the response code, removes (or not) a row from the table
	// Shows a Message bubble with the result.
	performDelete: function(code, rowID) {
		var xhrDelete = new XMLHttpRequest();
		var substanceIndexObj = this;
		
		xhrDelete.open("DELETE", "substance?code=" + code, true);
		xhrDelete.onreadystatechange = function () {
			if (xhrDelete.readyState == 4)
			{
				switch (xhrDelete.status)
				{
					case 200:	// HTTP OK
						substanceIndexObj.showMessage(substanceIndexObj.messages.delSuccess, false);
						substanceIndexObj.removeFromTable(rowID);
						break;
					case 400:	// HTTP Bad Request
						substanceIndexObj.showMessage(substanceIndexObj.messages.delBadRequest, true);
						break;
					case 404:	// HTTP Not Found
						substanceIndexObj.showMessage(substanceIndexObj.messages.delNotFound, true);
						substanceIndexObj.removeFromTable(rowID);
						break;
					default:	// Other code
						substanceIndexObj.showMessage(substanceIndexObj.messages.delUnknown, true);
				}
			}
		};
		xhrDelete.send(null);
	}
};

// This will initialize the SubstanceIndex object
window.addEventListener("DOMContentLoaded", function () {
	SubstanceIndex.table = document.getElementById('appSubstanceListTable');
	
	if (SubstanceIndex.table != null)
		SubstanceIndex.checkIfEmpty();
});

