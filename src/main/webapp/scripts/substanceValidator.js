//
//	SubstanceValidator object
//	Handles the validation of add/edit Substance form.
//
var SubstanceValidator = {
	// The FORM element this object works on.
	formElement:		null,
	isCodeFree:			true,
	isCodeValid:		false,
	isNameValid:		false,
	// Message strings to display under textboxes
	messages: {
		nameTooShort:	'Nazwa musi składać się z przynajmniej 3 znaków.',
		nameInvalid:	'Nazwa zawiera niedozwolone znaki.',
		codeCollides:	'Podany kod koliduje z: ',
		codeInvalid:	'Podany kod jest błędny.'
	},
	// IDs of the error message SPANs
	errorMsgElements:	[ 'formCodeErrorMessage', 'formNameErrorMessage' ],
	
	// Sets an error message SPAN with a error message provided
	setErrorMessage:	function (spanNo, errMsg) {
		if (spanNo == 0 || spanNo == 1)
		{
			var errorSPAN = document.getElementById(this.errorMsgElements[spanNo]);
			
			if (errMsg != null && errMsg.length > 0)
			{
				errorSPAN.innerHTML = errMsg;
				errorSPAN.style.display = "block";
			}
			else errorSPAN.style.display = "none";
		}
	},
	
	// Checks if there's already a substance with this code
	checkForCollision:	function () {
		var xhrCheck = new XMLHttpRequest();
		var validatorObj = this;
		var substanceCode = validatorObj.formElement.elements['code'].value;
		
		xhrCheck.open("GET", "check?code=" + substanceCode, true);
		xhrCheck.onreadystatechange = function () {
			if (xhrCheck.readyState == 4)
			{
				var originalCode = null;
			
				// If in edit mode, the form has a hidden "srcCode" field,
				// containing the original code of edited Substance.
				var srcCodeField = validatorObj.formElement.elements['srcCode'];
				if (srcCodeField)
					originalCode = srcCodeField.value;
			
				// If there was no such code in database (or an error occured),
				// the status code would change appropriately.
				if (xhrCheck.status == 200 && (originalCode == null || (originalCode != substanceCode)))
				{
					var collisionErrorMsg = validatorObj.messages.codeCollides;
					var collidingEntry = JSON.parse(xhrCheck.responseText);
					
					validatorObj.isCodeFree = false;					
					validatorObj.setErrorMessage(
						0, 
						collisionErrorMsg + (collidingEntry == null ? '(Brak nazwy)' : collidingEntry.name)
					);
				}
				else validatorObj.isCodeFree = true;
			}
		};
		xhrCheck.send(null);
	},
	
	// Checks ig the substance code entered is sane
	checkCodeSanity:	function() {
		var sanityPattern = /E(?:1[0-9]{2,3}|[2-9][0-9]{2})(?:[a-z]+|\([a-z]+\))?/g;
		var codeTrimmed = this.formElement.elements['code'].value.toString().trim();
		var patternMatch = codeTrimmed.match(sanityPattern);
		
		if (patternMatch != codeTrimmed)
		{
			this.setErrorMessage(0, this.messages.codeInvalid);
			this.isCodeValid = false;
		}
		else
		{
			this.setErrorMessage(0, null);
			this.isCodeValid = true;
		}
	},
	
	// Checks if the substance name entered is sane
	// Due to poor RegExp API in JS, we can only
	// check if the name is at least 3 chars long...
	checkNameSanity:	function() {
		var nameTrimmed = this.formElement.elements['name'].value.toString().trim();
		if (nameTrimmed.length < 3)
		{
			this.setErrorMessage(1, this.messages.nameTooShort);
			this.isNameValid = false;
		}
		else 
		{
			this.setErrorMessage(1, null);
			this.isNameValid = true;
		}
	},
	
	// Performs all checks on the code provided
	checkCode:			function() {
		this.checkCodeSanity();
		this.checkForCollision();
	}
};

// This initializes the SubstanceValidator object.
window.addEventListener("DOMContentLoaded", function () {
	SubstanceValidator.formElement = document.getElementById('appSubstanceForm');
	if (SubstanceValidator.formElement)
	{
		var theForm = SubstanceValidator.formElement;
		// Checks if the form is filled with valid data
		// Allows (or disallows) sending of the form
		theForm.onsubmit = function ()
		{
			SubstanceValidator.checkCodeSanity();
			SubstanceValidator.checkNameSanity();
			
			return (SubstanceValidator.isCodeValid && 
				SubstanceValidator.isCodeFree && 
				SubstanceValidator.isNameValid);
		};
		// Check correctness of the code each time
		// the user finishes typing in the text field.
		theForm.elements['code'].onchange = function ()
		{
			SubstanceValidator.checkCode();
		};
		// Same stuff for the name field.
		theForm.elements['name'].onchange = function ()
		{
			SubstanceValidator.checkNameSanity();
		};
	}
});
