<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pl.arma.entity.Message" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- The lastMessage session-object contains the result of the last operation --%>
<jsp:useBean id="lastMessage" class="pl.arma.entity.Message" scope="session" />
<!doctype html>
<html>
	<head>
		<title>Chemiczne dodatki do żywności</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="application.css" />
		<%-- This template can be provided with a list of mandatory scripts. --%>
		<%-- The ;-delimited list should be placed in includeScripts jsp:param. --%>
		<c:if test="${not empty param.includeScripts}">
			<c:forTokens items="${param.includeScripts}" delims=";" var="includedScript">
				<script type="text/javascript" src="scripts/${includedScript}"></script>
			</c:forTokens>
		</c:if>
	</head>
	<body>
		<div id="appHUD">
			<div id="appHUDInner">
				<form action="index.jsp">
					<button id="appHomeButton" class="appButton"></button>
				</form>
				<form action="index.jsp">
					<input name="what" type="text" placeholder="Wpisz kod lub częściową nazwę substancji..." />
					<button id="appSearchButton" class="appButton"></button>
				</form>
			</div>
		</div>
		<div id="appMainContainer">
			<%-- Display a correct type of message, error or success --%>
			<c:if test="${lastMessage.hasMessage()}">
				<c:choose>
					<c:when test="${lastMessage.errorFlag}">
						<c:set var="appMessageClassName" value="appErrorMessage" />
					</c:when>
					<c:otherwise>
						<c:set var="appMessageClassName" value="appSuccessMessage" />
					</c:otherwise>
				</c:choose>
				<div class="<c:out value="${appMessageClassName}" />" onclick="this.parentNode.removeChild(this)">
					<c:out value="${lastMessage.message}" />
				</div>
			</c:if>
