<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="pl.arma.entity.Substance" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
	editedSubstance is the request-scope container for submitted data
--%>
<jsp:useBean id="editedSubstance" class="pl.arma.entity.Substance" scope="request" />
<%-- 
	These two following properties will cause exceptions if invalid 
--%>
<c:catch>
	<jsp:setProperty name="editedSubstance" property="code" />
</c:catch>
<c:catch>
	<jsp:setProperty name="editedSubstance" property="name" />
</c:catch>
<%-- 
	These two will not 
--%>
<jsp:setProperty name="editedSubstance" property="usage" />
<jsp:setProperty name="editedSubstance" property="annotations" />
<%-- 
	This variable will only be set if editing an entry (or an error occured) 
--%>
<c:set var="originalSubstanceCode" value="${param.srcCode}" scope="request" />
<%--
	This JSP form includes form.jsp, which means that
	all data forwarded from the DataServlet will be exposed
	to the form.jsp, causing it to reappear in the form.
	This allows the user to correct errors and try again.
	Forwarding data to form.jsp, instead of to filled-form.jsp
	would result in a blank form (adding mode).
--%>
<jsp:include page="form.jsp" />
