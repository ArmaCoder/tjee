package pl.arma.db;

import java.util.ArrayList;
import pl.arma.entity.Substance;
import org.apache.commons.lang3.StringUtils;

public class SubstanceDatabase
{
	private ArrayList<Substance>	listByName;
	private ArrayList<Substance>	listByCode;

	public SubstanceDatabase()
	{
		listByName = new ArrayList<Substance>();
		listByCode = new ArrayList<Substance>();
	}

	//
	//	The adding method uses binary search to find
	//	the insertion place. Ordering should speed up
	//	searching for data.
	//
	public void add(Substance item) throws IllegalArgumentException
	{
		int insertCodeAt = (-1),
			insertNameAt = (-1);

		synchronized(this)
		{
			if (item.isValid() == false)
				throw new IllegalArgumentException("Dodawany element zawiera błędy.");
		
			for (int i = 0; i < listByName.size(); i++)
			{
				Substance	currentItemByCode = listByCode.get(i),
							currentItemByName = listByName.get(i);

				if (insertCodeAt == (-1) && 
					item.getCode().compareTo(currentItemByCode.getCode()) < 0)
				{
					insertCodeAt = i;
				}

				if (insertNameAt == (-1) && 
					item.getName().compareTo(currentItemByName.getName()) < 0)
				{
					insertNameAt = i;
				}

				if (insertCodeAt != (-1) && insertNameAt != (-1))
					break;
			}

			listByCode.add(insertCodeAt == (-1) ? listByCode.size() : insertCodeAt, item);
			listByName.add(insertNameAt == (-1) ? listByName.size() : insertNameAt, item);
		}
	}

	public void remove(Substance item)
	{
		synchronized(this)
		{
			listByCode.remove(item);
			listByName.remove(item);
		}
	}
	
	public void replace(Substance item1, Substance item2) throws IllegalArgumentException
	{
		synchronized(this)
		{
			if (listByName.indexOf(item1) == (-1)) return;
			if (item1.getCode().equals(item2.getCode()) || findByCode(item2.getCode()) == null)
			{
				remove(item1);
				add(item2);
			}
			else throw new IllegalArgumentException("Element o podanym kodzie już istnieje.");
		}
	}
	
	//
	//	This method finds a Substance with the code provided.
	//	The search is done using binary search and is case-insensitive.
	//
	public Substance findByCode(String code)
	{
		synchronized(this)
		{
			int	startIndex = 0,
				endIndex = listByCode.size() - 1,
				length = listByCode.size(),
				currentIndex = length / 2;

			while (length > 0)
			{
				Substance currentItem = listByCode.get(currentIndex);

				if (code.toLowerCase().compareTo(currentItem.getCode().toLowerCase()) > 0)	
				{
					startIndex++;
					length--;
					currentIndex = endIndex - (length / 2);
				}
				else if (code.toLowerCase().compareTo(currentItem.getCode().toLowerCase()) < 0)
				{
					endIndex--;
					length--;
					currentIndex = startIndex + (length / 2);
				}
				else
					return currentItem;
			}
			return null;
		}
	}

	//
	//	This method finds all records whose names include
	//	the text string provided.
	//
	public ArrayList<Substance> findByName(String name)
	{
		synchronized(this)
		{
			ArrayList<Substance> matchingSet = new ArrayList<Substance>();
			
			for (Substance sub : listByName)
			{
				if (StringUtils.containsIgnoreCase(sub.getName(), name))
					matchingSet.add(sub);
			}
			
			return matchingSet;
		}
	}

	public ArrayList<Substance> getListByCode()
	{
		return new ArrayList<Substance>(listByCode);
	}

	public ArrayList<Substance> getListByName()
	{
		return new ArrayList<Substance>(listByName);
	}
}
