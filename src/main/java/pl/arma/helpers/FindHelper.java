package pl.arma.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import pl.arma.entity.Substance;
import pl.arma.db.SubstanceDatabase;

public class FindHelper
{
	private SubstanceDatabase boundDataSource;

	public FindHelper(SubstanceDatabase dataSource)
	{
		boundDataSource = dataSource;
	}
	
	public ArrayList<Substance> find(String what)
	{
		ArrayList<Substance> matchingSet = new ArrayList<Substance>();
	
		// The code is valid, if it is in E100-E1999 range.
		// The code can also end with letters and parentheses with letters.
		if (Pattern.matches("E(1[0-9]{2,3}|[2-9][0-9]{2})([a-z]+|\\([a-z]+\\))?", what))
		{			
			Substance specificSubstance = boundDataSource.findByCode(what);
			if (specificSubstance != null)
				matchingSet.add(specificSubstance);
		}
		else matchingSet = boundDataSource.findByName(what);
		
		return matchingSet;
	}
}

