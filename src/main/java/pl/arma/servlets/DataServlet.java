package pl.arma.servlets;

import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.InvocationTargetException;
import pl.arma.entity.Message;
import pl.arma.entity.Substance;
import pl.arma.db.SubstanceDatabase;
import org.apache.commons.beanutils.BeanUtils;

@WebServlet("/substance")
public class DataServlet extends HttpServlet 
{
	//
	//	This method tries to retrieve information about a Substance
	//	that has the code provided in parameters, and:
	//	- either finds the Substance and passes it to filled-form.jsp for editing,
	//	- or doesn't find it, sets the last error message and forwards to index.jsp.
	//
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException 
	{
		Message failureMsg = new Message(null, true);	
		String substanceCode = request.getParameter("srcCode");
		
		if (substanceCode != null)
		{
			SubstanceDatabase sdb = (SubstanceDatabase) getServletContext().getAttribute("substanceDb");
			Substance requestedSubstance = sdb.findByCode(substanceCode);
			
			if (requestedSubstance != null)
			{
				request.setAttribute("editedSubstance", requestedSubstance);
				request.getRequestDispatcher("filled-form.jsp").forward(request, response);
				return;
			}
			else failureMsg.setMessage("Podana substancja nie istnieje.");
		}
		else failureMsg.setMessage("Nie podano kodu substancji.");

		// At this point the failure is certain, forward to index.jsp
		request.setAttribute("lastMessage", failureMsg);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
	
	//
	//	This method processes the data posted from the JSP form, and:
	//	- if the original record's code (srcCode) is present - tries to update it,
	//	- if there's not srcCode argument - tries to add a new record.
	//	If everything succeeds, the request is forwarded to the blank JSP form (form.jsp),
	//	if not - the request is forwarded to filled-form.jsp, which puts
	//	all the submitted data back into the JSP form to correct and re-send.
	//	This method uses Apache BeanUtils to automate property-to-bean mapping.
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException 
	{
		Message resultMsg = new Message();
		Substance submittedSubstance = new Substance();
		SubstanceDatabase sdb = (SubstanceDatabase) getServletContext().getAttribute("substanceDb");
		
		try
		{
			BeanUtils.populate(submittedSubstance, request.getParameterMap());			
			if (submittedSubstance.isValid())
			{
				// The srcCode parameter means the original code
				// of the edited Substance record.
				String srcCode = request.getParameter("srcCode");
				if (srcCode != null)
				{
					Substance replacedSubstance = sdb.findByCode(srcCode);
					if (replacedSubstance != null)
						sdb.replace(replacedSubstance, submittedSubstance);
					else
						resultMsg.setMessage("Nie znaleziono wskazanego rekordu.");
				}
				else sdb.add(submittedSubstance);
			}
			else resultMsg.setMessage("Przesłano błędne i/lub niekompletne dane.");
		}
		catch (InvocationTargetException e)
		{
			resultMsg.setMessage("Przesłano błędne dane (" + e.getCause().getMessage() + ")");
		}
		catch (Exception e)
		{
			resultMsg.setMessage(e.getMessage());
		}
		// At the beginning, resultMsg was blank.
		// If something's happened (resultMsg has a message), 
		// an error must have occured.
		if (resultMsg.hasMessage())
		{
			resultMsg.setErrorFlag(true);
			request.setAttribute("lastMessage", resultMsg);
			// filled-form.jsp exposes the submitted data to form.jsp
			// (form.jsp is included at the end of filled-form.jsp),
			// making all the submitted data reappear in the form.
			request.getRequestDispatcher("filled-form.jsp").forward(request, response);
		}
		else 
		{
			resultMsg.setMessage("Pomyślnie zapisano zmiany.");
			request.setAttribute("lastMessage", resultMsg);
			request.getRequestDispatcher("form.jsp").forward(request, response);
		}
	}

	//
	//	This method responds to DELETE commands.
	//	Because DELETEs are sent via AJAX, this only responds with a code.
	//
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException 
	{
		String substanceCode = request.getParameter("code");
		if (substanceCode == null)
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		else
		{
			SubstanceDatabase sdb = (SubstanceDatabase) getServletContext().getAttribute("substanceDb");
			Substance deletedSubstance = sdb.findByCode(substanceCode);
			
			if (deletedSubstance == null)
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			else
				sdb.remove(deletedSubstance);
		}
	}
	
	@Override
	public void init()
	{
		// This initializes and puts an instance of SubstanceDatabase
		// for the JSPs in the session scope.
		ServletContext sCtx = getServletContext();
		if (sCtx.getAttribute("substanceDb") == null)
			sCtx.setAttribute("substanceDb", new SubstanceDatabase());
	}
}

