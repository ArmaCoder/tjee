package pl.arma.servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pl.arma.entity.Substance;
import pl.arma.db.SubstanceDatabase;
import com.google.gson.*;

@WebServlet("/check")
public class CheckServlet extends HttpServlet 
{
	//
	//	This method makes the CheckServlet respond to GETs with
	//	either: information about a substance with the code given,
	//	or: an HTTP error code if not found/no code given.
	//
	//	Gson is used to print Substance entities in JSON format.
	//
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException 
	{
		response.setContentType("application/json; charset=UTF-8");
		
		String substanceCode = request.getParameter("code");
		if (substanceCode == null)
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		else
		{
			SubstanceDatabase sdb = (SubstanceDatabase) getServletContext().getAttribute("substanceDb");
			Substance requestedSubstance = sdb.findByCode(substanceCode);
			
			if (requestedSubstance == null)
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			else
			{
				response.getWriter().print(new GsonBuilder()
					.excludeFieldsWithoutExposeAnnotation()
					.create()
					.toJson(requestedSubstance)
				);
			}
		}
	}
	
	@Override
	public void init()
	{
		// This initializes and puts an instance of SubstanceDatabase
		// for the JSPs in the session scope.
		ServletContext sCtx = getServletContext();
		if (sCtx.getAttribute("substanceDb") == null)
			sCtx.setAttribute("substanceDb", new SubstanceDatabase());
	}
}
