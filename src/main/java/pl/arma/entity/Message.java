package pl.arma.entity;
import org.apache.commons.lang3.StringEscapeUtils;

public class Message
{
	private String message;
	private boolean isError = false;
	private boolean wasRetrieved = false;
	
	public Message()
	{
	}
	
	public Message(String msg)
	{
		setMessage(msg);
	}
	
	public Message(String msg, boolean err)
	{
		this(msg);
		isError = err;
	}
	
	public boolean hasMessage()
	{
		return (message != null && !wasRetrieved);
	}
	
	public void setMessage(String msg)
	{
		message = msg;
		wasRetrieved = false;
	}
	
	public String getMessage()
	{
		if (!wasRetrieved)
		{
			wasRetrieved = true;
			return StringEscapeUtils.escapeHtml4(message);
		}
		else return null;
	}
	
	public void setErrorFlag(boolean err)
	{
		isError = err;
	}
	
	public boolean getErrorFlag()
	{
		return isError;
	}
}
