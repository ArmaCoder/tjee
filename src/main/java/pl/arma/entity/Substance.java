package pl.arma.entity;
import java.util.regex.Pattern;
import com.google.gson.annotations.*;
import org.apache.commons.lang3.StringEscapeUtils;

public class Substance
{
	@Expose private String code;
	@Expose private String name;
	private String usage		= "";
	private String annotations	= "";
	
	public boolean isValid()
	{
		return (code != null && name != null);
	}
	
	public String getCode()
	{
		return code;
	}

	public void setCode(String newCode) throws IllegalArgumentException
	{
		String trimmedNewCode = newCode.trim();
		// The code is valid, if it is in E100-E1999 range.
		// The code can also end with letters and parentheses with letters.
		if (Pattern.matches("E(1[0-9]{2,3}|[2-9][0-9]{2})([a-z]+|\\([a-z]+\\))?", trimmedNewCode))
			code = trimmedNewCode;
		else
			throw new IllegalArgumentException("Błędny format kodu substancji.");
	}

	public String getName()
	{
		return name;
	}

	public void setName(String newName) throws IllegalArgumentException
	{
		String trimmedName = newName.trim();
	
		if (newName.length() >= 3)
		{
			// A valid name consists only of alphabetic
			// characters, numbers and spaces.
			if (Pattern.matches("[\\p{IsAlphabetic}0-9\\s]+", trimmedName))
				name = trimmedName;
			else
				throw new IllegalArgumentException("Nazwa zawiera niedozwolone znaki.");
		}
		else throw new IllegalArgumentException("Nazwa jest za krótka (ma mniej niż 3 znaki).");
	}

	public String getUsage()
	{
		// Escaped, to avoid placing HTML on site.
		return StringEscapeUtils.escapeHtml4(usage);
	}

	public void setUsage(String uses)
	{
		usage = uses;
	}

	public String getAnnotations()
	{
		// Escaped, to avoid placing HTML on site.
		return StringEscapeUtils.escapeHtml4(annotations);
	}

	public void setAnnotations(String text)
	{
		annotations = text;
	}
	
	public void assign(Substance src) throws IllegalArgumentException
	{
		if (src.isValid())
		{
			setCode(src.getCode());
			setName(src.getName());
			setUsage(src.getUsage());
			setAnnotations(src.getAnnotations());
		}
		else throw new IllegalArgumentException("Przypisywany obiekt posiada błędy.");
	}
}
